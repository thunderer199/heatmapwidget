'use strict';

describe('Service: Helper', function () {

  // load the controller's module
  beforeEach(module('heatmapApp'));

  var service;
  // Initialize the controller and a mock scope
  beforeEach(inject(function (helper) {
    service = helper;
  }));

  // check militaryTimeToRegular

  it('should convert military time to regular 0 to 12am', function () {
    var time = service.militaryTimeToRegular(0);
    expect(time).toBe('12 am');
  });

  it('should convert military time to regular 1 to 1am', function () {
    var time = service.militaryTimeToRegular(1);
    expect(time).toBe('1 am');
  });

  it('should convert military time to regular 11 to 11am', function () {
    var time = service.militaryTimeToRegular(11);
    expect(time).toBe('11 am');
  });

  it('should convert military time to regular 13 to 1pm', function () {
    var time = service.militaryTimeToRegular(13);
    expect(time).toBe('1 pm');
  });

  // color

  it('gradient test: blue color ', function () {
    expect(service.color(510, 0, 0)).toEqual([0, 0, 255]);
  });


  it('gradient test: green color ', function () {
    expect(service.color(510, 0, 128)).toEqual([0, 255, 0]);
  });

  it('gradient test: yellow color ', function () {
    expect(service.color(510, 0, 255)).toEqual([255, 255, 0]);
  });

  it('gradient test: yellow color 2 ', function () {
    expect(service.color(1268, 0, 634)).toEqual([255, 255, 0]);
  });

  it('gradient test: red color ', function () {
    expect(service.color(255, 0, 255)).toEqual([255, 0, 0]);
  });


});
