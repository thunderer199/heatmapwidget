/**
 * @ngdoc overview
 * @name heatmapApp
 * @description
 * # heatmapApp
 *
 * Main module of the application.
 */
angular
  .module('heatmapApp', []);
