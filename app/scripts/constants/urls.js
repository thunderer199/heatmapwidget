angular.module('heatmapApp')
  .constant('urls', {
    heatData: '/api/data/HeatMapData.json',
    heatDataModified: '/api/data/HeatMapDataModified.json',
    heatDataError: '/api/data/HeatMapDataError.json',
    notFound: '/api/data/'
  });
