angular.module('heatmapApp')
  .constant('settings', {
    nameOfDays: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
    hoursType: [' am', ' pm']
  });
