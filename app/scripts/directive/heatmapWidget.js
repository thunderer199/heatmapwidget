angular.module('heatmapApp')
  .directive('heatmapWidget', function () {
    return {
      restrict: 'E',
      controller: 'HeatmapWidgetCtrl',
      templateUrl: '/template/widgetTemplate.html',
      scope: {indexName: '@', axisX: '@', axisY: '@', objectName: '@', url: '@'}
    };
  });
