angular.module('heatmapApp')
  .service('helper', function (settings) {
    'use strict';

    this.getAllHours = function () {
      var arr = [];
      for (var i = 0; i < 24; i++) {
        arr[i] = this.militaryTimeToRegular(i);
      }
      return arr;
    };

    this.getAllDays = function () {
      return settings.nameOfDays;
    };

    /**
     * Convert 24-hour presentation of data to 12-hour
     * @param hour hours in 24-hour format
     * @returns {string} hours in 12-hour format
     */
    this.militaryTimeToRegular = function (hour) {
      var type = settings.hoursType;
      var h = ((11 + hour) % 12 + 1);
      return h + type[Math.floor(hour / 12)];
    };

    /**
     * Convert timestamp to required format
     * @param {number} timestamp
     * @returns {{day: string, hours: string}}
     */
    this.convertTimestamp = function (timestamp) {
      var date = new Date(timestamp);
      var hours = this.militaryTimeToRegular(date.getHours());
      var day = this.dayNumToShortName(date.getDay());
      return {'day': day, 'hours': hours};
    };

    /**
     * Get short name of day of week by it number
     * @param dayNum Day number
     * @returns {string} Short name oo day of week
     */
    this.dayNumToShortName = function (dayNum) {
      var names = settings.nameOfDays;
      return names[dayNum - 1];
    };

    /**
     * Return color for value in range
     * @param {number} max Max value of data set
     * @param {number} min Min value of data set
     * @param {number} value Current relative value
     * @returns {number[]} color in array of rgb palette [red, green, blue]
     */
    this.color = function (max, min, value) {
      // min is blue, middle is yellow, max is red
      var blue = 0, green = 0, red = 0;
      var numberOfChunks = 510;
      var numInChunk = (max - min) / numberOfChunks;
      var x = Math.floor(value / numInChunk);

      // blue(0) -> yellow(128) -> green(255) -> red(510)
      if (x < 128) {
        x *= 2;
        blue = 255 - x;
        green = x;
      } else if (x < 255) {
        green = 255;
        red = (x - 128) * 2;
      } else {  // x <= 510
        red = 255;
        green = 510 - x;
      }
      // concat color
      return [red, green, blue];
    };

  });
