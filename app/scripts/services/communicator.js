angular.module('heatmapApp')
  .service('communicator', function ($http, $q, $timeout) {
    'use strict';

    this.getHeatmapData = function (url) {
      var deferred = $q.defer();

      $timeout(function () {
          $http({
            method: 'GET',
            url: url
          }).then(function (response) {
            var data = response.data;
            console.log(data);
            if (response.status === 200 && data !== undefined && data.error === 0) {
              deferred.resolve(data);
            } else {
              deferred.reject('Internal server error');
            }

          }).catch(function (data) {
            return deferred.reject(data.statusText);
          });
        }, 2000
      );

      return deferred.promise;
    };

  });
