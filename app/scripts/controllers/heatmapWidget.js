/**
 * @ngdoc function
 * @name heatmapApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the heatmapApp
 */
angular.module('heatmapApp')
  .controller('HeatmapWidgetCtrl', function ($scope, urls, communicator, helper) {
    'use strict';

    var that = this;
    that.dataToViewData = convertData;

    //default values
    var objName = $scope.objectName || "Site Incident Heat Severity Map (lv12)";
    var indexType = $scope.indexType || 'main';
    var axisX = $scope.axisX || 'dowId';
    var axisY = $scope.axisY || 'hourId';

    that.isLoading = true;
    that.errors = [];
    communicator.getHeatmapData($scope.url).then(function (data) {

      var normalizedData = that.dataToViewData(data.result, objName, indexType, axisX, axisY);
      var computeColor = helper.color.bind(null, normalizedData.sysinfo.maxValue, normalizedData.sysinfo.minValue);

      that.viewData = normalizedData.data;

      var axis = {
        x: axisX === 'dowId' ? helper.getAllDays() : helper.getAllHours(),
        y: axisY === 'dowId' ? [''].concat(helper.getAllDays()) : helper.getAllHours()
      };

      that.getXAxisLabel = function (idx) {
        return axis.x[idx];
      };

      that.getYAxisLabels = function () {
        return axis.y;
      };

      that.colorize = function (val) {
        var color = computeColor(val).join(',') + ', 255';
        if (val === null || val === undefined) {
          color = '0, 0, 0, 0'; // for cells without data
        }
        return 'rgba(' + color + ');';
      };

    }).catch(function (data) {
      that.errors = [data];
    }).finally(function () {
      that.isLoading = false;
    });

    /**
     * Transform data to required for view format
     * @param {Object} data
     * @param {string} objName name of the property under the "data" object
     * @param {string} indexType name of inject in data, don't use if index contains axis names
     * @param {string} xAxisName name of x axis
     * @param {string} yAxisName name of y axis
     * @returns {Object} transform date
     */
    function convertData(data, objName, indexType, xAxisName, yAxisName) {
      var resulting = [];

      var sys = {maxValue: Number.MIN_VALUE, minValue: Number.MAX_VALUE};
      data.forEach(function (obj) {
        var defaultVal = obj.index.hasOwnProperty(xAxisName) && obj.index.hasOwnProperty(yAxisName);
        var index = defaultVal ? obj.index : obj.index[indexType];

        var xAxis = index[xAxisName];
        var yAxis = index[yAxisName];

        // because days starts from 1, not 0 as hours
        if (xAxisName === 'dowId') {
          xAxis -= 1;
        }

        obj.data[objName].forEach(function (objData) {
          if (resulting[xAxis] === undefined) {
            resulting[xAxis] = [];
          }
          resulting[xAxis][yAxis] = objData.value;
          sys.maxValue = Math.max(sys.maxValue, objData.value);
          sys.minValue = Math.min(sys.minValue, objData.value);
        });
      });


      return {data: resulting, sysinfo: sys};
    }


  });
