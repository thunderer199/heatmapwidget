angular.module('heatmapApp')
  .controller('MainCtrl', function (urls) {
    'use strict';

    var that = this;
    that.notFoundUrl = urls.notFound;
    that.heatData = urls.heatData;
    that.heatDataModified = urls.heatDataModified;

  });
